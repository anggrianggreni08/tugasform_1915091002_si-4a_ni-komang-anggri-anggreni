<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>Form Output</title>
</head>

<body>
    <h1 class="text-center pt-3">OUTPUT NILAI SISWA/SISWI</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 border mt-3 p-3">
                <table class="table border">
                    <?php
                    $Nama = $_POST['Nama'];
                    $Mapel = $_POST['Mapel'];
                    $UTS = $_POST['UTS'];
                    $UAS = $_POST['UAS'];
                    $Tugas = $_POST['Tugas'];

                    $NT = (0.35 * $UTS) + (0.50 * $UAS) + (0.15 * $Tugas);

                    if ($NT <= 50 && $NT >= 0) {
                        $GN = 'D';
                    } else if ($NT > 50 && $NT <= 70) {
                        $GN = 'C';
                    } else if ($NT > 70 && $NT < 90) {
                        $GN = 'B';
                    } else if ($NT >= 90 && $NT <= 100) {
                        $GN = 'A';;
                    }
                    
                    ?>
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nama Siswa/Siswa</th>
                            <th scope="col">Mata Pelajaran</th>
                            <th scope="col">Nilai UTS</th>
                            <th scope="col">Nilai UAS</th>
                            <th scope="col">Nilai Tugas</th>
                            <th scope="col">Total Nilai</th>
                            <th scope="col">Grade Nilai</th></tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td><?php echo ($Nama); ?></td>
                            <td><?php echo ($Mapel); ?></td>
                            <td><?php echo ($UTS); ?></td>
                            <td><?php echo ($UAS); ?></td>
                            <td><?php echo ($Tugas); ?></td>
                            <td><?php echo ($NT); ?></td>
                            <td><?php echo ($GN); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</body>

</html>